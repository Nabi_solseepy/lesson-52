import React, { Component } from 'react';

import './Hoax.css';
class Hoax extends Component{
    render() {
        return(
         <div className="Hoax">
             <p>{this.props.number}</p>
         </div>
        );
    }
}
export default Hoax;