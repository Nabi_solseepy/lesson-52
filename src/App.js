import React, { Component } from 'react';

import Hoax from './components/hoax/Hoax';
import './App.css';

class App extends Component {
    state = {
      numbers:[]
    };

    getRandomNamber = () => {
        let arrNumber = [];
        while (arrNumber.length < 5) {
            const number = Math.floor(Math.random() * (36 - 5) + 5);
            if(arrNumber.indexOf(number) === -1) {
                arrNumber.push(number)
            }
        }

        arrNumber.sort(function (a, b) {
            return a - b;
        });
       this.setState({numbers: arrNumber})
    };

    render() {
        return (
            <div className="App">
            <div>
                <button className="click" onClick={this.getRandomNamber}>click</button>
            </div>

                {this.state.numbers.map((number, index) => {
                    return (
                        <Hoax key={index} number={number}/>
                    )
                })}



            </div>
        )

    }
}


export default App;